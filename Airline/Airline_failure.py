import FetchData
import DB_Credentials
import Update
import Write
import Pipeline
import sys

# DB_Credential '0' for test '1' for production
credential = DB_Credentials.Credentials(sys.argv[1])
print("Hourly Update for KPIs")
today = FetchData.dpdata().get_cw_today(credential, int(sys.argv[2]), int(sys.argv[3]))
# print(today)
today_og = Pipeline.Stream().Create_today(today)
# print(today_og)
dfa,dfr = Pipeline.Stream().segregate(today_og, credential)
Update.Updating().UpdateRecords(dfa, credential)
dfr = Write.Writing().Records(dfr)
Write.Writing().write_airline_failure_rate(dfr, credential)  # insert Records in Database
