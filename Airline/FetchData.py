import psycopg2
from datetime import timedelta
import pandas as pd

# path = "Airline/"
path = ""

class dpdata:
    # Get DP Data
    def get_cw_today(self, Credential,startday,endday):
        print("start")
        date = (pd.datetime.now()- timedelta(hours=1)).replace(minute=00, second=00, microsecond=00, hour=00) - timedelta(startday)
        print(date)
        date2 = pd.datetime.now().replace(minute=00, second=00, microsecond=00) - timedelta(seconds=1,days=endday)
        print(date2)
        # Get Main Data
        try:
            conn = psycopg2.connect(Credential)
        except psycopg2.DatabaseError as ex:
            raise Exception("Unable to Connect to DB")
        file = open(path + 'airline_failure.sql', 'r', encoding='utf-8-sig')
        sql = " ".join(file.readlines())
        curs = conn.cursor()
        curs.execute(sql.format(str(date), str(date2)))
        NewData = list(curs)
        curs.close()
        conn.close()
        return NewData

    def segregate(self, Credential):
        print("start")
        # Get Main Data
        try:
            conn = psycopg2.connect(Credential)
        except psycopg2.DatabaseError as ex:
            raise Exception("Unable to Connect to DB")
        file = open(path + 'Segregate.sql', 'r', encoding='utf-8-sig')
        sql = " ".join(file.readlines())
        curs = conn.cursor()
        curs.execute(sql)
        NewData = list(curs)
        curs.close()
        conn.close()
        return NewData