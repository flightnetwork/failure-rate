import datetime
import psycopg2
from datetime import datetime

class Writing:

    def Records(self, dt):
       df=dt.copy()
       label = ['datetime', 'AL', 'Booked', 'Failed', 'Booking_Failure_Rate','totalbooking']
       df['datetime'] = df['datetime'].astype(str)
       l =  list (df.columns ^ label)
       for i in l:
           df[i]=0
       df=df[label]
       zipped_vals = zip(df['datetime'],df['AL'],df['Booked'],df['Failed'],df['Booking_Failure_Rate'],df['totalbooking'])
       return(zipped_vals)


    def write_airline_failure_rate(self, data, credential):
        start_time = datetime.now()
        conn = psycopg2.connect(credential)
        conn.autocommit = True
        curs = conn.cursor()

        for row in data:
            try:
                DataInsert = """ INSERT INTO airline_failure_rate (datetime, al, booked, failed, booking_failure_rate,totalbooking) 
                VALUES """ + str(row) + ";"
                curs.execute(DataInsert)
                conn.commit()
            except Exception as e:
                print(e)
        curs.close()
        conn.close()
        end_time = datetime.now()
        print("writing to airline_failure_rate Table took => ", end_time - start_time)
