import requests
import json
from enum import Enum
import slack


class COLOR(Enum):
    GREEN = "#36a64f"
    RED = "#ff0000"
    ORANGE = "#ff9900"


class SlackNotifications:

    def __init__(self, webhook=None, isEnabledNotifications=False):
        self.headers = {
            "Content-Type": "application/json",
            "Authorization": 'Bearer xoxp-2914229649-170301378757-591251190704-98aefb9ba124099c4506cd6c66ad4130',
            "charset": "utf-8"
        }
        self.webhook = "https://hooks.slack.com/services/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"
        self.isEnabledNotifications = isEnabledNotifications

    def post_image(self, image, channel):

        f = {'file': (image, open(image, 'rb'), 'image/png', {'Expires': '0'})}

        return requests.post(

            url='https://slack.com/api/files.upload',
            data={'token': 'xoxb-2914229649-584955437697-7sCRYDZorfRS50yvpaK2LyJX',
                  'channels': channel,
                  'media': f,
                  },
            headers={'Accept': 'application/json'},
            files=f,
        )

    def post_text(self, text, channel):

        slack_token = 'xoxb-2914229649-584955437697-7sCRYDZorfRS50yvpaK2LyJX'
        sc = slack.WebClient(slack_token)
        sc.api_call('chat.postMessage',
                    json={'channel': channel, 'text': text})

    def notify_simple(self, message):
        payload = {"text": message}
        response = self.submit_notification_request(payload=payload)

        self.verify_response(response=response)

    def submit_notification_request(self, payload):

        return requests.post(
            url=self.webhook,
            data=json.dumps(payload),
            headers=self.headers,
            timeout=10)

    def verify_response(self, response):
        if response.status_code != 200:
            print(
                "Failed to submit Slack notification. "
                "Status:", response.status_code, "Response:", response.text)
