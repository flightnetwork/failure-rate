import pandas as pd
import numpy as np
import FetchData


class Stream:

    def Create_today(self, df):
        labels = ['datetime', 'original_booking_status', 'airline']
        df = pd.DataFrame.from_records(df, columns=labels)
        # try:
        df["Length"] = df['airline'].str.len()
        df.loc[df['original_booking_status'] != 'Booked', 'original_booking_status'] = 'Failed'
        df = df[df.airline != '']
        df = df[df.Length < 7]
        df = df.drop(['Length'], axis=1)
        df[['AL1', 'AL2']] = df.airline.str.split("/", expand=True)
        df1 = df[['datetime', 'original_booking_status',  'AL1']]
        df2 = df[['datetime', 'original_booking_status',  'AL2']]
        df2 = df2.dropna(axis=0, subset=['AL2'])
        df3 = df1.append(df2)
        df3 = df3[['datetime', 'original_booking_status', 'AL1', 'AL2']]
        df3 = df3.fillna('')
        df3['AL'] = np.where((df3['AL1'] == ''), df3['AL2'],
                             df3['AL1'])
        df3 = df3.drop(['AL1', 'AL2'], axis=1)

        # Time Ops
        df3['datetime'] = pd.to_datetime(df3.datetime)
        df3['Bookings'] = 1
        dt = pd.pivot_table(df3, index=["datetime","AL"], values=["Bookings"],
                            columns=['original_booking_status'], aggfunc=[np.sum], fill_value=0)
        dt.columns = dt.columns.droplevel()
        dt.columns = dt.columns.droplevel()
        dt = dt.reset_index()
        dt['totalbooking']=(dt['Booked']+dt['Failed'])
        dt['Booking_Failure_Rate'] = (dt['Failed']/(dt['Booked']+dt['Failed'])*100)
        return dt
    # Remove Current Hour
    def Remove_Current_Hr(self, df):
        date = pd.datetime.now().replace(minute=00, second=00, microsecond=00)
        print(date)
        df = df[df.datetime < date]
        return df


    def segregate(self, df, credential):
        dt = FetchData.dpdata().segregate(credential)
        dt = pd.DataFrame.from_records(dt, columns=['datetime', 'AL'])
        dt['concat'] = dt['datetime'].astype(str) + dt['AL'].astype(str)
        df['concat'] = df['datetime'].astype(str) + df['AL'].astype(str)
        dfa = df[df.concat.isin(dt['concat'])]
        dfr = df[~ df.concat.isin(dt['concat'])]
        dfa = dfa.drop('concat', axis=1)
        dfr = dfr.drop('concat', axis=1)
        return dfa, dfr