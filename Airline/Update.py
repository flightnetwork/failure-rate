from datetime import datetime
import psycopg2


class Updating:

    def UpdateRecords(self, dt, Credential):
        print("Updating to airline_failure_rate Table at " + str(datetime.now()))
        conn = psycopg2.connect(Credential)
        conn.autocommit = True
        curs = conn.cursor()
        Table = 'airline_failure_rate'
        index = len(dt.index)
        for ind in range(index):
            try:
                DataInsert = """ UPDATE """ + str(Table) + \
                             """ SET  Booked = """ + str((dt.iloc[ind]['Booked'])) + \
                             """ , Failed = """ + str((dt.iloc[ind]['Failed'])) + \
                             """ , Booking_Failure_Rate = """ + str((dt.iloc[ind]['Booking_Failure_Rate'])) + \
                             """ , totalbooking = """ + str((dt.iloc[ind]['totalbooking'])) + \
                             """ WHERE datetime = '""" + str(dt.iloc[ind]['datetime']) + \
                             """' and AL = '""" + str((dt.iloc[ind]['AL'])) + \
                             """';"""
                curs.execute(DataInsert)
                conn.commit()
            except Exception as e:
                print("ext")
                print(e)
        curs.close()
        conn.close()
        print("Done Updating to airline_failure_rate Table at " + str(datetime.now()))
